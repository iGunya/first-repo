public class MoveSummCred extends TestBankCalc {

    static int numberTestOneMethod=1; //test 3, в массиве нумерация с 0
    static int numberTestTwoMethod=2; //test 3, в массиве нумерация с 0

    public static void fieldSumm() {
       movePage.valuefildSummCredit("300 000");
        //elementField.clear();после clear начальное значение возвращается

        res.set(numberTestOneMethod,movePage.valButtonNext().equals("300 000 ₽"));
    }

    public static void sliderSumm() {
        movePage.sliderFildSummCredit(600);

        boolean check=true ;
        //привязка к значению над кнопкоц "Далее"
        check &=movePage.valButtonNext().equals("100 000 000 ₽");
        //приязка к полю "Сумма кредита"
        check &=movePage.valuefildSummCredit().equals("100 000 000");
        res.set(numberTestTwoMethod, check);
    }
}