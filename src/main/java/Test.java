import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class Test {

    public static void openChrome() throws MalformedURLException {
        WebDriver driver = new RemoteWebDriver(new URL("http://localhost:3579/wd/hub"),new ChromeOptions());
        driver.manage().window().maximize();
        driver.get("https://www.softlab.ru/");

        WebElement elementnInput = driver.findElement(By.xpath("//form//input[1]"));
        elementnInput.clear();
        elementnInput.sendKeys("Современные технологии");

        WebElement elementnButtonFind = driver.findElement(By.xpath("//input[@type=\"submit\"]"));
        elementnButtonFind.click();

        //driver.close();
    }

    public static void main(String[] args) {
        //тесты
        CountBank test =new CountBank();
        TestBankCalc.openCheckWeb();             // test 1
        MoveSummCred.fieldSumm();               // tret 2
        CountBank.countIndroduceCridit();       //test 4
        MoveSummCred.sliderSumm();            //test 3
        //закрываем браузер
        TestBankCalc.getDriver().close();

        //запись результатв в файл
        WorkOnFile.openFile(TestBankCalc.getRes());
    }
}