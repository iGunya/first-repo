import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import page.PageBankCalck;

import java.util.ArrayList;
import java.util.Collections;


public class TestBankCalc {
    static ArrayList<Boolean> res = new ArrayList<Boolean>(Collections.nCopies(4,true));
    static PageBankCalck movePage = new PageBankCalck();

    protected static int numTestOne=0;//test1, но массив с 0
    public static void openCheckWeb() {

        movePage.openWeb();

        try {//если какой нибудь элемент не найден поизойдет исключение, тест буде отмечен как невыполненный
            //проверка наличия поля "Сумма кредита"
            movePage.fildSummCredit();

            // роверка выпадающего поля валют
            WebElement slider = movePage.sliderCurrency();
            slider.click();
            if (movePage.countIntroduceCurrency() != 3) //3 варианта валюты
                throw new Exception();
            slider.click();

            //проверка выпадающегего списка "Срок"
            movePage.fieldLength();

            //проверка наличия поля "Ставка"
            movePage.fieldRate();

            //проверка наличия кнопки "Дальше"
            movePage.buttonNext();

        }catch (Exception e){
            res.set(numTestOne, false);
            return;
        }

        //запись результата в массив
        res.set(numTestOne, true);

    }

    public static WebDriver getDriver() {
        return movePage.getDriver();
    }

    public static ArrayList<Boolean> getRes() {
        return res;
    }
}


