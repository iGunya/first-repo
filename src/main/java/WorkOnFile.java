import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTShd;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STShd;
import sun.awt.geom.AreaOp;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class WorkOnFile {

    static void setColor(XWPFTableCell cell, String rgbStr) {
        CTTc ctTc = cell.getCTTc();
        CTTcPr tcpr = ctTc.isSetTcPr() ? ctTc.getTcPr() : ctTc.addNewTcPr();
        CTShd ctshd = tcpr.isSetShd() ? tcpr.getShd() : tcpr.addNewShd();
        ctshd.setColor("auto");
        ctshd.setVal(STShd.CLEAR);
        ctshd.setFill(rgbStr);

        if (ctshd.isSetThemeFill()) ctshd.unsetThemeFill();
        if (ctshd.isSetThemeFillShade()) ctshd.unsetThemeFillShade();
        if (ctshd.isSetThemeFillTint()) ctshd.unsetThemeFillTint();
    }

     public static void openFile(ArrayList<Boolean> res) {
        try {
            // get file
            FileInputStream input = new FileInputStream("TK_creditCalc.docx");
            XWPFDocument document = new XWPFDocument(input);
            System.out.println("файл открыт");
            //get table in file
            List<XWPFTable> tables = document.getTables();
            XWPFTable table=tables.get(0);
            //set result test
            for(int i=0; i < res.size();i++) {
                XWPFTableRow row = table.getRow(i+1);
                XWPFTableCell cell = row.getCell(6);//6-row with result test

                if ((res.get(i))) {
                    setColor(cell, "00FF00");
                } else {
                    setColor(cell, "FF0000");
                }
                //System.out.print(cell.getText());
            }
            FileOutputStream out = new FileOutputStream("./TK_creditCalc_result.docx");
            document.write(out);
            out.close();
        } catch (FileNotFoundException e) {
            System.out.print("файл не открыт");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.print("не открыт");
            e.printStackTrace();
        }

    }
}
