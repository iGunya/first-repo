package page;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class OtherMove {
    // удаляет из поля если не удаляется clear
    public void charField(WebElement clear, String str){
        clear.sendKeys(Keys.CONTROL + "a");
        clear.sendKeys(Keys.DELETE);
        clear.click();
        clear.sendKeys( str);
    }
}
