package page;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PageBankCalck {

    private final OtherMove move =new OtherMove();

    private WebDriver driver;

    public WebDriver getDriver() {
        return driver;
    }

    //в кострукторе создаём драйвер
    public PageBankCalck() {
        try {
            this.driver = new RemoteWebDriver(new URL("http://localhost:3579/wd/hub"),new ChromeOptions());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        // открываем страничку на полный экран
        this.driver.manage().window().maximize();
    }
    // открываем сайт
    public void openWeb(){
        driver.get("https://www.banki.ru/services/calculators/credits/");
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    //получение поля "Сумма кредита"
    public WebElement fildSummCredit(){
        return driver.findElement(By.xpath
                ("//div[text() = 'Сумма кредита']/parent::label/following::div[1]"));
    }
    public void valuefildSummCredit(String str){ // изменеие значениея поля "Сумма кредита"
        WebElement clear = driver.findElement(By.xpath
                ("//div//input[@value=\"100 000\"]"));
        move.charField(clear,str);
    }
    public String valuefildSummCredit(){ // значение поля "Сумма кредита"
        return driver.findElement(By.xpath
                ("//input[@type=\"tel\"]")).getAttribute("value");
    }
    public void sliderFildSummCredit(int val){
        WebElement elementSlider = driver.findElement(By.xpath("//div[@class=\"tWYyi\"]//span"));

        Actions moveSlider = new Actions(driver);
        Action action = moveSlider.dragAndDropBy(elementSlider, val, 0).build();
        action.perform();
    }

    // получение выпадающего поля валют
    public WebElement sliderCurrency(){
        return driver.findElement(By.xpath("//input[@value = \"RUB\"]/.."));
    }
    public int countIntroduceCurrency(){
        List<WebElement> currency = driver.findElements(By.xpath
                ("//div[contains(text(),\"₽\")]|//div[contains(text(),\"$\")]|//div[contains(text(),\"€\")]"));
        return currency.size();
    }

    // получчнеие поля "Срок"
    public WebElement fieldLength(){
         return driver.findElement(By.xpath
                ("//div[text() = 'Срок']/../following::div[1]//span[@type=\"arrow-down\"]"));
    }
    public WebElement moveFieldLength(String mount){
        return driver.findElement(By.xpath("//ul//li/div[text() = '"+mount+"']"));
    }
    // получчнеие поля "Ставка"
    public WebElement fieldRate(){
        return driver.findElement(By.xpath
                ("//div[text() = 'Ставка, %']/../following::div[1]"));
    }
    public void moveFieldRate(String str){
        WebElement clear = driver.findElement(By.xpath
                ("//input[@type=\"text\"]"));
        move.charField(clear,str);

    }

    // получчнеие кнопки "Далее"
    public WebElement buttonNext(){
        return driver.findElement(By.xpath("//span[text() = 'Дальше']"));
    }
    public String valButtonNext(){ // значение над кнопкой "Далее"
        return driver.findElement(By.xpath("//div//b")).getText();
    }
    // всего подобранно кредитов
    public String findBank(){
        return driver.findElement(By.xpath("//span[@class=\"text-weight-bolder text-size-6\"]")).getText();
    }
}
